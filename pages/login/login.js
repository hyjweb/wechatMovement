Page({
  data: {
    pickerHidden: true,
    chosen: '',
    username: '请填写您的姓名',
    code: '请填写您组织代码',
    title: '欢迎关注保亿集团微信计步',
  },
  pickerConfirm: function (e) {
    this.setData({
      pickerHidden: true
    })
    this.setData({
      chosen: e.detail.value
    })
  },
  pickerCancel: function (e) {
    this.setData({
      pickerHidden: true
    })
  },
  pickerShow: function (e) {
    this.setData({
      pickerHidden: false
    })
  },
  formSubmit: function (e) {
    var warn = "";//弹框时提示的内容  
    var flag = true;//判断信息输入是否完整  
    //判断的顺序依次是：姓名-手机号-地址-具体地址-预约日期-预约时间-开荒面积  
    if (e.detail.value.username == "") {
      warn = "请填写您的姓名！";
    } else if (e.detail.value.code == "") {
      warn = "请填写您的组织号码！";
    } else {
      flag = false;
      var username = e.detail.value.username;
      var code = e.detail.value.code;
      // wx.showToast({
      //   icon: 'loading',
      //   duration: 1000
      // });
      wx.switchTab({ url: '../../pages/home/home' });
      console.log('form发生了submit事件，携带数据为：', e.detail.value)
    }


    //如果信息填写不完整，弹出输入框  
    if (flag == true) {
      wx.showModal({
        title: '提示',
        content: warn
      })
    }
  },
  formReset: function (e) {
    console.log('form发生了reset事件，携带数据为：', e.detail.value)
    wx.navigateTo({ url: '../../pages/index/index' });
  }
})
