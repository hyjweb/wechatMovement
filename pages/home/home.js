//获取应用实例
const app = getApp()

Page({
  data: {
    curBdIndex: 0,
    currentTab: 0,
    handleTab:0,
    second_height: "0px",
    department: '中国科技中心',
    selecteddepart: '中国科技中心',
    job: '工程师',
    num:'5005',
    companyshow:'公司榜',
    departmentshow:'部门榜',
    objectMultiArray: [
      [
       { id: 0, name: '财务部'},
       { id: 1, name: '设计部'},
       { id: 2, name: '施工部' },
       { id: 3, name: '业务部' },
       { id: 4, name: '销售部' }
      ], 
      [
        { id: 0, name: '部门1'},
        { id: 1, name: '部门2'},
        { id: 2, name: '部门3'},
        { id: 3, name: '部门4'},
        { id: 3, name: '部门5'}
      ], 
      [
        {id: 0,name: '小组1'},
        {id: 1,name: '小组2'}
      ]
    ],
    multiIndex: [1, 1, 1],
    walkdata:[
      { id: "5", score: "1", face: '/assate/rest.jpg', date: "2017-03-02", num: "8000", name:"成大爷" },
      { id: "2", score: "2", face: '/assate/test2.jpg', date: "2017-03-02", num: "7000", name: "成大爷" },
      { id: "3", score: "3", face: '/assate/rest.jpg', date: "2017-03-02", num: "6000", name: "成大爷" },
      { id: "4", score: "4", face: '/assate/test2.jpg', date: "2017-03-02", num: "5000", name: "成大爷" },
      { id: "1", score: "5", face: '/assate/rest.jpg', date: "2017-03-02", num: "4000", name: "成大爷" },
      { id: "6", score: "5", face: '/assate/test2.jpg', date: "2017-03-02", num: "3000", name: "成大爷" },
      { id: "7", score: "6", face: '/assate/rest.jpg', date: "2017-03-02", num: "2000", name: "成大爷" },
      { id: "8", score: "7", face: '/assate/test2.jpg', date: "2017-03-02", num: "1000", name: "成大爷" },
      { id: "9", score: "1212", face: '/assate/test2.jpg', date: "2017-03-02", num: "500", name: "成大爷" },
      { id: "10", score: "12121", face: '/assate/test2.jpg', date: "2017-03-02", num: "200", name: "成大爷" },
      { id: "11", score: "454545", face: '/assate/test2.jpg', date: "2017-03-02", num: "0", name: "成大爷" },
    ]
    
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    };
    // 获取系统信息
    this.showHeight();
    // console.log(this.data.objectMultiArray[0][1].name)
    // console.log(this.data.objectMultiArray[1][1].name)
    // console.log(this.data.objectMultiArray[2][1].name)
    // console.log(this.data.multiArray[2][multiIndex[2]])

  },
  //获取部门
  actionHandel:function(e){
    var that = this;
    wx.showActionSheet({
      itemList: that.data.departments,
      success: function (res) {
        that.setData({
          selecteddepart: that.data.departments[res.tapIndex]
        })
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },
  //显影
  swichNav: function (e) {
    this.setData({
      curBdIndex: e.target.dataset.current,
      currentTab: e.target.dataset.current
    })
  },

  onHide: function (e) {
    this.setData({
      second_height: 0
    })
  },

  onShow: function (e) {
    this.showHeight();
  },

  showHeight: function () {
    wx.getSystemInfo({
      success: res => {
        this.setData({
          second_height: res.windowHeight - (res.windowWidth / 750) * 260
        })
      }
    });
  },

  gsbHandle: function(){
    this.setData({
      handleTab: 0,
    })
  },
  
  bmbHandle: function () {
    this.setData({
      handleTab: 1,
    })
  },

  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
  }
})
