const util = require('../../utils/util.js')
//获取应用实例
const app = getApp()
Page({
  data: {
    isHiddenToast: true,
    department: '中国科技中心',
    selecteddepart: '中国科技中心',
    job: '工程师',
    num: '5005',
    date:[],
    second_height: "0px",
    walkNum: '3030300',
    dayinfo:[
      { id: '1', day: '01', week: '星期一', walk: '3561', sort: '589', isTodayLarge: true },
      { id: '2', day: '02', week: '星期二', walk: '5656', sort: '512', isTodayLarge: true },
      { id: '3', day: '03', week: '星期三', walk: '4541', sort: '543', isTodayLarge: true  },
      { id: '4', day: '04', week: '星期四', walk: '3424', sort: '523', isTodayLarge: true },
      { id: '5', day: '05', week: '星期五', walk: '0', sort: '531121', isTodayLarge: true  },
      { id: '6', day: '06', week: '星期六', walk: '', sort: '', isTodayLarge: false },
      { id: '7', day: '07', week: '星期日', walk: '', sort: '', isTodayLarge: false  },
      { id: '8', day: '08', week: '星期一', walk: '', sort: '', isTodayLarge: false }
    ],

    maxdata:[],
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
 
 //时间选择器
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value
    })
  },

  onLoad: function () { 
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    };


    var date = util.formatmonth(new Date());  
    this.setData({ date: date  });  

    var that = this;
    that.showHeight();

    var dayinfo =  this.data.dayinfo;
    var arr = [];
    for (var index in dayinfo) {
      if (dayinfo[index].walk && dayinfo[index].walk !=""){
        var b = parseInt(dayinfo[index].walk);
        arr.push(b);
      }
    }

    Array.prototype.max = function () {
      that.setData({
        maxdata: Math.max.apply({}, this)
      })
    }
    arr.max();
    
    this.setData({
      isHiddenToast: false
    })
  },
  //显影
  swichNav: function (e) {
    this.setData({
      curBdIndex: e.target.dataset.current,
      currentTab: e.target.dataset.current
    })
  },
  
  onHide: function (e) {
    this.setData({
      second_height: 0
    })
  },

  onShow: function (e) {
    this.showHeight();
  },

  showHeight: function(){
    wx.getSystemInfo({
      success: res => {
        this.setData({
          second_height: res.windowHeight - (res.windowWidth / 750) * 460
        })
      }
    }); 
  }

})
